import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../models/models.dart';

class PrincipalPage extends StatefulWidget {
  const PrincipalPage({super.key});

  @override
  State<PrincipalPage> createState() => _PrincipalPageState();
}

class _PrincipalPageState extends State<PrincipalPage> {
  @override
  void initState() {
    obtenerCategoria();
    obtenerProducto("1");

    super.initState();
  }

  List<Categoria> oListaCategoria = [];
  List<Producto> oListaProducto = [];

  void obtenerCategoria() async {
    var dio = Dio();
    final response =
        await dio.get('https://api.escuelajs.co/api/v1/categories');

    // oListaEmpresa:List<Empresa>.from(oNuevaListaEmpresa.map((x) => Empresa.fromJson(x))),

    oListaCategoria =
        List<Categoria>.from(response.data.map((x) => Categoria.fromJson(x)));

    print('obtenerCategoria');
    setState(() {});
  }

  void obtenerProducto(String nCategoria) async {
    var dio = Dio();
    final response = await dio
        .get('https://api.escuelajs.co/api/v1/categories/$nCategoria/products');

    // oListaEmpresa:List<Empresa>.from(oNuevaListaEmpresa.map((x) => Empresa.fromJson(x))),

    oListaProducto =
        List<Producto>.from(response.data.map((x) => Producto.fromJson(x)));

    print('obtenerProducto');
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    //print(oListaCategoria[0].name.toString());
    print('object en build');

    //if (oListaCategoria.isEmpty || oListaProducto.isEmpty) {

    //}
    return Scaffold(
      body: Stack(
        children: [
          Container(color: Colors.grey[200]),
          Padding(
            padding: const EdgeInsets.only(top: 80, left: 20, right: 20),
            child: Row(
              //mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    //alignment: Alignment.centerRight,
                    width: 320,
                    height: 65,
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(18),
                        border: Border.all(width: 1.0, color: Colors.black)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20, right: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text("Search",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 18.00,
                                //fontWeight: FontWeight.bold,
                              )),
                          IconButton(
                              // style:ButtonStyle(backgroundColor:),
                              iconSize: 35.00,
                              color: Colors.black,
                              onPressed: () {},
                              icon: const Icon(Icons.search)),
                        ],
                      ),
                    )),
                IconButton(
                  iconSize: 35.00,
                  color: Colors.black,
                  onPressed: () {},
                  icon: const Icon(Icons.filter_alt),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 180, left: 20, right: 20),
            child: (oListaCategoria.isNotEmpty
                ? SingleChildScrollView(
                    child: SizedBox(
                    height: 120.0,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: oListaCategoria.length,
                        itemBuilder: ((context, index) {
                          return Column(children: [
                            Row(children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 15),
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      List<Producto> oListaProducto = [];
                                    });
                                    obtenerProducto(
                                        oListaCategoria[index].id.toString());
                                  },
                                  child: CircleAvatar(
                                    radius: 40,
                                    backgroundImage: NetworkImage(
                                        oListaCategoria[index]
                                            .image
                                            .toString()),
                                  ),
                                ),
                              ),
                            ]),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(oListaCategoria[index].name.toString(),
                                style: const TextStyle(
                                    // color: Colors.blueAccent,
                                    fontSize: 17.00,
                                    fontWeight: FontWeight.bold)),
                          ]);
                        })),
                  ))
                : const LinearProgressIndicator()),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 320, left: 20, right: 20),
              child: Container(
                alignment: Alignment.center,
                height: 120,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20.00),

                  /*borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.00),
                        topRight: Radius.circular(20.00))*/
                ),
                child: ListTile(
                  // tileColor: Colors.red,
                  title: const Text('50% OFF',
                      style: TextStyle(
                          // color: Colors.blueAccent,
                          fontSize: 25.00,
                          fontWeight: FontWeight.bold)),
                  subtitle: const Text('on all women' 's shoes'),
                  leading: Container(
                    height: 60,
                    width: 60,
                    //color: Colors.grey[200],
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(15.00),

                      /*borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.00),
                        topRight: Radius.circular(20.00))*/
                    ),
                    child: IconButton(
                      iconSize: 35.00,
                      color: Colors.black,
                      onPressed: () {},
                      icon: const Icon(Icons.new_releases),
                    ),
                  ),
                  trailing: IconButton(
                    iconSize: 35.00,
                    color: Colors.grey,
                    onPressed: () {},
                    icon: const Icon(Icons.arrow_forward_ios_outlined),
                  ),
                ),
              )),
          const Padding(
            padding: EdgeInsets.only(top: 460, left: 20, right: 20),
            child: Text('New Items',
                style: TextStyle(
                    // color: Colors.blueAccent,
                    fontSize: 28.00,
                    fontWeight: FontWeight.bold)),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 500, left: 20, right: 20),
            child: (oListaProducto.isNotEmpty
                ? SingleChildScrollView(
                    child: SizedBox(
                    height: 250.0,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: oListaProducto.length,
                        itemBuilder: ((context, index) {
                          return Column(children: [
                            Row(children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 20),
                                child: Stack(children: [
                                  Container(
                                      width: 170,
                                      height: 200,
                                      //color: Colors.amber,
                                      decoration: BoxDecoration(
                                        color: Colors.grey[200],
                                        borderRadius:
                                            BorderRadius.circular(15.00),
                                      ),
                                      child: ((oListaProducto[index]
                                              .images!
                                              .isEmpty
                                          ? Container()
                                          : ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(25),
                                              child: Image(
                                                fit: BoxFit.cover,
                                                image: NetworkImage(
                                                    oListaProducto[index]
                                                        .images![0]
                                                        .toString()),
                                              ),
                                            )))),
                                  Positioned(
                                    top: 10,
                                    left: 100,
                                    child: Container(
                                      width: 50,
                                      height: 50,
                                      //color: Colors.amber,
                                      decoration: BoxDecoration(
                                        color: Colors.grey[200],
                                        shape: BoxShape.circle,
                                      ),
                                      child: const Icon(Icons.favorite,
                                          color: Colors.black, size: 30),
                                    ),
                                  ),
                                ]),

                                /*  radius: 40,
                            backgroundImage: NetworkImage(
                                oListaCategoria[index].image.toString()),*/
                              ),
                            ]),
                            const SizedBox(
                              height: 10,
                            ),
                            Column(
                                // crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      oListaProducto[index]
                                          .title
                                          .toString()
                                          .trim()
                                          .substring(
                                              0,
                                              (oListaProducto[index]
                                                          .title
                                                          .toString()
                                                          .length >
                                                      10
                                                  ? 10
                                                  : oListaProducto[index]
                                                      .title
                                                      .toString()
                                                      .length)),
                                      // textAlign: TextAlign.left,
                                      style: const TextStyle(
                                          color: Colors.grey,
                                          fontSize: 17.00,
                                          fontWeight: FontWeight.bold)),
                                  Text('S/ ${oListaProducto[index].price}',
                                      // textAlign: TextAlign.left,
                                      style: const TextStyle(
                                          color: Colors.grey,
                                          fontSize: 17.00,
                                          fontWeight: FontWeight.bold)),
                                ]),
                          ]);
                        })),
                  ))
                : LinearProgressIndicator()),
          ),

          /* Padding(
            padding: const EdgeInsets.only(top: 520, left: 20, right: 20),
            child: SingleChildScrollView(
                child: SizedBox(
              height: 120.0,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: oListaCategoria.length,
                  itemBuilder: ((context, index) {
                    return Column(children: [
                      Row(children: [
                        Padding(
                            padding: const EdgeInsets.only(right: 20),
                            child: Container(
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              width: 100,
                              //  height: 100.0,
                              // color: Colors.amber,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(30),
                                    child: Image(
                                        width: 10,
                                        height: 10,
                                        fit: BoxFit.cover,
                                        image: AssetImage(oListaCategoria[index]
                                            .image
                                            .toString())),
                                  ),
                                ],
                              ),
                            )
                            /*CircleAvatar(
                            radius: 40,
                            backgroundImage: NetworkImage(
                                oListaCategoria[index].image.toString()),
                          ),*/
                            ),
                      ]),
                      /* const SizedBox(
                        height: 10,
                      ),
                      Text(oListaCategoria[index].name.toString(),
                          style: TextStyle(
                              // color: Colors.blueAccent,
                              fontSize: 17.00,
                              fontWeight: FontWeight.bold)),*/
                    ]);
                  })),
            )),
          ),*/
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: double.infinity,
              height: 110.00,
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(40.00),

                /*borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.00),
                        topRight: Radius.circular(20.00))*/
              ),
              child: Column(mainAxisAlignment: MainAxisAlignment.end,
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        //mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          IconButton(
                            iconSize: 35.00,
                            color: Colors.grey,
                            onPressed: () {},
                            icon: const Icon(Icons.home),
                          ),
                          IconButton(
                            iconSize: 35.00,
                            color: Colors.grey,
                            onPressed: () {},
                            icon: const Icon(Icons.favorite),
                          ),
                          IconButton(
                            iconSize: 35.00,
                            color: Colors.grey,
                            onPressed: () {},
                            icon: const Icon(Icons.card_travel_outlined),
                          ),
                          IconButton(
                            iconSize: 35.00,
                            color: Colors.grey,
                            onPressed: () {},
                            icon: const Icon(Icons.person),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 160.00,
                      height: 5.00,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(50.00)),
                    ),
                    const SizedBox(
                      height: 10,
                    )
                  ]),
            ),
          )
        ],
      ),
    );
  }
}
