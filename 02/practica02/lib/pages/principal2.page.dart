import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../models/models.dart';

class PrincipalPage2 extends StatefulWidget {
  const PrincipalPage2({super.key});

  @override
  State<PrincipalPage2> createState() => _PrincipalPage2State();
}

class _PrincipalPage2State extends State<PrincipalPage2> {
  @override
  void initState() {
    obtenerCategoria();
    super.initState();
  }

  List<Categoria> oListaCategoria = [];
  List<Producto> oListaProducto = [];

  void obtenerCategoria() async {
    print('obtenerCategoria');
    var dio = Dio();
    final response =
        await dio.get('https://api.escuelajs.co/api/v1/categories');

    // oListaEmpresa:List<Empresa>.from(oNuevaListaEmpresa.map((x) => Empresa.fromJson(x))),
    print(response.data);
    oListaCategoria =
        List<Categoria>.from(response.data.map((x) => Categoria.fromJson(x)));

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    print('object in build');
    return Scaffold(
        body: Stack(children: [
      Container(color: Colors.grey[200]),
      Padding(
        padding: const EdgeInsets.only(top: 200, left: 20, right: 20),
        child: SingleChildScrollView(
            child: SizedBox(
          height: 120.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: oListaCategoria.length,
              itemBuilder: ((context, index) {
                return Column(children: [
                  Row(children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: CircleAvatar(
                        radius: 40,
                        backgroundImage: NetworkImage(
                            oListaCategoria[index].image.toString()),
                      ),
                    ),
                  ]),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(oListaCategoria[index].name.toString(),
                      style: const TextStyle(
                          // color: Colors.blueAccent,
                          fontSize: 17.00,
                          fontWeight: FontWeight.bold)),
                ]);
              })),
        )),
      ),
    ]));
  }
}
