import 'package:flutter/material.dart';

import '../pages/pages.dart';

class AppRoute {
  static const rutaInicial = '/principal';

  static final rutaOpciones = <Ruta>[
    Ruta(
        sNombreOpcion: 'principal',
        oOpcion: const PrincipalPage(),
        sRuta: '/principal',
        oIcono: Icons.home),
    Ruta(
        sNombreOpcion: 'principal2',
        oOpcion: const PrincipalPage2(),
        sRuta: '/principal2',
        oIcono: Icons.home),
    /* Ruta(
        sNombreOpcion: 'detalle',
        oOpcion: const Detalle(),
        sRuta: '/detalle',
        oIcono: Icons.rule_outlined),*/
    /* Ruta(
        sNombreOpcion: 'menu-opciones',
        oOpcion: const MenuOpciones(),
        sRuta: '/seguridad/menu-opciones',
        oIcono: Icons.rule_outlined),
    // CLIENTE
    Ruta(
        sNombreOpcion: 'busqueda-cliente',
        oOpcion: const BusquedaCliente(),
        sRuta: '/cliente/busqueda-cliente',
        oIcono: Icons.contact_mail_outlined),
    Ruta(
        sNombreOpcion: 'registro-persona-natural',
        oOpcion: const RegistroPersonaNatural(),
        sRuta: '/cliente/registro-persona-natural',
        oIcono: Icons.contact_mail_outlined),
    Ruta(
        sNombreOpcion: 'registro-persona-juridica',
        oOpcion: const RegistroPersonaJuridica(),
        sRuta: '/cliente/registro-persona-juridica',
        oIcono: Icons.contact_mail_outlined)*/
    // CLIENTE
  ];

  static Map<String, Widget Function(BuildContext)> getRutas() {
    Map<String, Widget Function(BuildContext)> oRutas = {};

    for (var element in rutaOpciones) {
      oRutas.addAll({element.sRuta: (BuildContext context) => element.oOpcion});
    }

    return oRutas;
  }

  // static Map<String, Widget Function(BuildContext)> rutas ={
  //   'inicio-sesion'   : (BuildContext context) =>const InicioSesion(),
  //   'empresa-perfil'   : (BuildContext context) =>const ListaEmpresaPerfilScreen()
  // } ;
}

class Ruta {
  final String sNombreOpcion;
  final Widget oOpcion;
  final String sRuta;
  final IconData oIcono;

  Ruta(
      {required this.sNombreOpcion,
      required this.oOpcion,
      required this.sRuta,
      required this.oIcono});
}
